FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app
COPY . .
RUN apt-get update && apt-get install make 
RUN make build
RUN make test 

FROM build AS publish
RUN make publish

FROM base AS final
COPY --from=publish app/publish .
ENTRYPOINT ["dotnet", "api-sds.dll"]