#Variables
.PHONY: build
build:
	dotnet build --configuration Release ./src/api-sds/ -o build

.PHONY: clean
clean: 
	rm -rf ./src/api-sds/bin
	rm -rf ./src/api-sds/obj
	rm -rf ./src/api-sds/logs

.PHONY: restore
restore:
	dotnet restore ./src/api-sds/api-sds.csproj
# .PHONY: ru
# run: 
# 	dotnet run -p ./src/api-sds/
.PHONY: test 
test:
	dotnet test ./test/api-sds.tests/api-sds.tests.csproj

.PHONY: publish
publish:
	dotnet publish ./src/api-sds/ -o publish

.PHONY: run
run: 
	cd ./src/api-sds/ && dotnet run 


.PHONY: build_push
build_push:
	cd ./scripts/ && ./local_build_push.sh

.PHONY: create_stack
create_stack:
	cd ./scripts/ && ./local_create_stack.sh
	
