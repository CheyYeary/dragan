
#login ecr is needed for local development login to the ECR service before pushing or pulling an image
function login_ECR () {
    loginToken="$(aws ecr get-login --no-include-email --region us-west-2)"
    eval "$loginToken"
}

function build_ECR () {
    eval "aws ecr create-repository --region us-west-2 --repository-name  benefits-portal"
}

#create cloud formation stack for ecs/fargate. Cluster,Service,Task
function create_stack() {
eval "aws cloudformation create-stack --stack-name benefits-portal-api-sds --template-body file://$PWD/../cloudformation/benefits-cluster.yml"
}

#Updates cloud formation stack for ecs/fargate. 
function update_stack() {
    eval "aws cloudformation update-stack --stack-name benefits-portal-api-sds --template-body file://$PWD/../cloudformation/benefits-cluster.yml"

}