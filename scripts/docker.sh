
# docker build to push is one method where all arguments can be put in to be the only entry point for a build and push to ECR. Local use only for now.
function docker_build_to_push() {
    imageName=$1
    tagName=$2
    docker_build $imageName
    docker_tag $tagName
    docker_push_ecr $tagName
}

# docker build runs the docker build command to create an image with the name provided.
function docker_build() {
    imageName=$1
    eval "docker build -t api-sds ."
}

# docker tag will create a new tag associated with the base image.
function docker_tag() {
    dockerTag=$1
    eval "docker tag api-sds:latest 359158632008.dkr.ecr.us-west-2.amazonaws.com/benefits-portal:$dockerTag"
}

# docker push ecr will push the image with the correct tag to the container registry.
function docker_push_ecr() {
    dockerTag=$1
    eval "docker push 359158632008.dkr.ecr.us-west-2.amazonaws.com/benefits-portal:$dockerTag"
}